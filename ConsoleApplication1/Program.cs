﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReadWriteMemory;
using System.Diagnostics;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var RWM = new ReadWriteMemory64("notepad");
            if (!RWM.OpenProcess(MemoryHelper.GetdwDesiredAccess()))
                return;
            Console.WriteLine(RWM.ImageAddress());
        }
    }
}
