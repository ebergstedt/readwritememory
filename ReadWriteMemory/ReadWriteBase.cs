﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace ReadWriteMemory
{
    public abstract class ReadWriteBase
    {        
        protected ProcessModule MyProcessModule;

        //these might be used in the future
        protected const ulong PAGE_EXECUTE = 16;
        protected const ulong PAGE_EXECUTE_READ = 32;
        protected const ulong PAGE_EXECUTE_READWRITE = 64;
        protected const ulong PAGE_EXECUTE_WRITECOPY = 128;
        protected const ulong PAGE_GUARD = 256;
        protected const ulong PAGE_NOACCESS = 1;
        protected const ulong PAGE_NOCACHE = 512;
        protected const ulong PAGE_READONLY = 2;
        protected const ulong PAGE_READWRITE = 4;
        protected const ulong PAGE_WRITECOPY = 8;

        public readonly string ProcessName;
        public readonly int ProcessID;
        public readonly Process MyProcess;

        public int ProcessHandle;

        protected ReadWriteBase(int processId)
        {
// ReSharper disable ConditionIsAlwaysTrueOrFalse
            ProcessID = Process.GetProcessById(processId) != null ? Process.GetProcessById(processId).Id : -1;
// ReSharper restore ConditionIsAlwaysTrueOrFalse
            if (ProcessID == -1) 
                return;
            MyProcess = Process.GetProcessById(ProcessID);
            ProcessName = MyProcess.ProcessName;
        }

        protected ReadWriteBase(string processName)
        {
            //TODO: handle multiple processes with same name
            MyProcess = Process.GetProcessesByName(processName).FirstOrDefault();
            if (MyProcess != null)
                ProcessID = MyProcess.Id;
            else
                ProcessID = -1;
        }

        public bool CheckProcess()
        {
            return (Process.GetProcessesByName(ProcessName).Length > 0);
        }

        [DllImport("kernel32.dll")]
        protected static extern int OpenProcess(uint dwDesiredAccess, bool bInheritHandle, int dwProcessId);
        public bool OpenProcess(uint dwDesiredAccess)
        {
            if (ProcessID == -1)
            {
                Console.WriteLine(@"ERROR: Your process """ + ProcessName + @""" does not exist.");
                return false;
            }
            if (MyProcess.Id == 0)
            {
                Console.WriteLine(ProcessName + " is not running or has not been found. Please check and try again", "Process Not Found");
                return false;
            }
            ProcessHandle = OpenProcess(dwDesiredAccess, false, MyProcess.Id);
            if (ProcessHandle == 0)
            {
                Console.WriteLine(ProcessName + " is not running or has not been found. Please check and try again", "Process Not Found");
                return false;
            }
            return true;
        }

        [DllImport("kernel32.dll")]
        protected static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, out byte[] buffer, int size, out int lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        protected static extern bool ReadProcessMemory(long hProcess, long lpBaseAddress, out byte[] buffer, long size, out long lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        protected static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress, byte[] buffer, int size, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        protected static extern bool WriteProcessMemory(long hProcess, long lpBaseAddress, byte[] buffer, long size, out long lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        protected static extern bool CloseHandle(int hObject);

        [DllImport("kernel32.dll")]
        protected static extern bool CloseHandle(long hObject);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        protected static extern int FindWindowByCaption(int ZeroOnly, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        protected static extern long FindWindowByCaption(long ZeroOnly, string lpWindowName);

        [DllImport("kernel32.dll")]
        protected static extern bool VirtualProtectEx(int hProcess, int lpAddress, int dwSize, uint flNewProtect, out uint lpflOldProtect);

        [DllImport("kernel32.dll")]
        protected static extern bool VirtualProtectEx(long hProcess, long lpAddress, long dwSize, ulong flNewProtect, out ulong lpflOldProtect);        
    }
}
