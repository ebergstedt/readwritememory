﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace ReadWriteMemory
{
    public class ReadWriteMemory32 : ReadWriteBase
    {
        protected int BaseAddress;

        public ReadWriteMemory32(int processID)
            : base(processID)
        {
        }

        public ReadWriteMemory32(string processName)
            : base(processName)
        {
        }

        public int DllImageAddress(string dllname)
        {
            foreach (ProcessModule procmodule in MyProcess.Modules)
            {
                if (dllname == procmodule.ModuleName)
                {
                    return (int)procmodule.BaseAddress;
                }
            }
            return -1;
        }

        public int ImageAddress()
        {
            this.BaseAddress = 0;
            this.MyProcessModule = this.MyProcess.MainModule;
            this.BaseAddress = (int)this.MyProcessModule.BaseAddress;
            return this.BaseAddress;
        }

        public int ImageAddress(int pOffset)
        {
            this.BaseAddress = 0;
            this.MyProcessModule = this.MyProcess.MainModule;
            this.BaseAddress = (int)this.MyProcessModule.BaseAddress;
            return (pOffset + this.BaseAddress);
        }

        public int Pointer(bool AddToImageAddress, int pOffset)
        {
            return this.ReadInt(this.ImageAddress(pOffset));
        }

        public MemoryHelper.ByteMem32 ReadMem(int pOffset, int pSize)
        {
            byte[] buffer = new byte[pSize];
            int lpNumberOfBytesRead;
            ReadProcessMemory(base.ProcessHandle, pOffset, out buffer, pSize, out lpNumberOfBytesRead);
            return new MemoryHelper.ByteMem32() { Buffer = buffer, LpNumberOfBytesRead = lpNumberOfBytesRead };
        }

        public MemoryHelper.ByteMem32 ReadMem(int pOffset, int pSize, bool AddToImageAddress)
        {
            byte[] buffer = new byte[pSize];
            int lpNumberOfBytesRead;
            int lpBaseAddress = AddToImageAddress ? this.ImageAddress(pOffset) : pOffset;
            ReadProcessMemory(base.ProcessHandle, lpBaseAddress, out buffer, pSize, out lpNumberOfBytesRead);
            return new MemoryHelper.ByteMem32() { Buffer = buffer, LpNumberOfBytesRead = lpNumberOfBytesRead };
        }

        public int WriteMem(int pOffset, byte[] pBytes)
        {
            int lpNumberOfBytesWritten;
            WriteProcessMemory(base.ProcessHandle, pOffset, pBytes, pBytes.Length, out lpNumberOfBytesWritten);
            return lpNumberOfBytesWritten;
        }

        public int WriteMem(int pOffset, byte[] pBytes, bool AddToImageAddress)
        {
            int lpBaseAddress = AddToImageAddress ? this.ImageAddress(pOffset) : pOffset;
            int lpNumberOfBytesWritten;
            WriteProcessMemory(base.ProcessHandle, lpBaseAddress, pBytes, pBytes.Length, out lpNumberOfBytesWritten);
            return lpNumberOfBytesWritten;
        }

        public int Pointer(string Module, int pOffset)
        {
            return this.ReadInt(this.DllImageAddress(Module) + pOffset);
        }

        public int Pointer(bool AddToImageAddress, int pOffset, int pOffset2)
        {
            if (AddToImageAddress)
                return (this.ReadInt(this.ImageAddress() + pOffset) + pOffset2);
            else
                return (this.ReadInt(pOffset) + pOffset2);
        }

        public int Pointer(string Module, int pOffset, int pOffset2)
        {
            return (this.ReadInt(this.DllImageAddress(Module) + pOffset) + pOffset2);
        }

        public int Pointer(bool AddToImageAddress, int pOffset, int pOffset2, int pOffset3)
        {
            return (this.ReadInt(this.ReadInt(this.ImageAddress(pOffset)) + pOffset2) + pOffset3);
        }

        public int Pointer(string Module, int pOffset, int pOffset2, int pOffset3)
        {
            return (this.ReadInt(this.ReadInt(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3);
        }

        public int Pointer(bool AddToImageAddress, int pOffset, int pOffset2, int pOffset3, int pOffset4)
        {
            return (this.ReadInt(this.ReadInt(this.ReadInt(this.ImageAddress(pOffset)) + pOffset2) + pOffset3) + pOffset4);
        }

        public int Pointer(string Module, int pOffset, int pOffset2, int pOffset3, int pOffset4)
        {
            return (this.ReadInt(this.ReadInt(this.ReadInt(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3) + pOffset4);
        }

        public int Pointer(bool AddToImageAddress, int pOffset, int pOffset2, int pOffset3, int pOffset4, int pOffset5)
        {
            return (this.ReadInt(this.ReadInt(this.ReadInt(this.ReadInt(this.ImageAddress(pOffset)) + pOffset2) + pOffset3) + pOffset4) + pOffset5);
        }

        public int Pointer(string Module, int pOffset, int pOffset2, int pOffset3, int pOffset4, int pOffset5)
        {
            return (this.ReadInt(this.ReadInt(this.ReadInt(this.ReadInt(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3) + pOffset4) + pOffset5);
        }

        public int Pointer(bool AddToImageAddress, int pOffset, int pOffset2, int pOffset3, int pOffset4, int pOffset5, int pOffset6)
        {
            return (this.ReadInt(this.ReadInt(this.ReadInt(this.ReadInt(this.ReadInt(this.ImageAddress(pOffset)) + pOffset2) + pOffset3) + pOffset4) + pOffset5) + pOffset6);
        }

        public int Pointer(string Module, int pOffset, int pOffset2, int pOffset3, int pOffset4, int pOffset5, int pOffset6)
        {
            return (this.ReadInt(this.ReadInt(this.ReadInt(this.ReadInt(this.ReadInt(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3) + pOffset4) + pOffset5) + pOffset6);
        }

        public byte ReadByte(int pOffset)
        {
            byte[] buffer = new byte[1];
            int lpNumberOfBytesRead;
            ReadProcessMemory(base.ProcessHandle, pOffset, out buffer, 1, out lpNumberOfBytesRead);
            return buffer[0];
        }

        public byte ReadByte(bool AddToImageAddress, int pOffset)
        {
            byte[] buffer = new byte[1];
            int lpNumberOfBytesRead;
            int lpBaseAddress = AddToImageAddress ? this.ImageAddress(pOffset) : pOffset;
            ReadProcessMemory(base.ProcessHandle, lpBaseAddress, out buffer, 1, out lpNumberOfBytesRead);
            return buffer[0];
        }

        public byte ReadByte(string Module, int pOffset)
        {
            byte[] buffer = new byte[1];
            int lpNumberOfBytesRead;
            ReadProcessMemory(base.ProcessHandle, this.DllImageAddress(Module) + pOffset, out buffer, 1, out lpNumberOfBytesRead);
            return buffer[0];
        }

        public float ReadFloat(int pOffset)
        {
            return BitConverter.ToSingle(this.ReadMem(pOffset, 4).Buffer, 0);
        }

        public float ReadFloat(bool AddToImageAddress, int pOffset)
        {
            return BitConverter.ToSingle(this.ReadMem(pOffset, 4, AddToImageAddress).Buffer, 0);
        }

        public float ReadFloat(string Module, int pOffset)
        {
            return BitConverter.ToSingle(this.ReadMem(this.DllImageAddress(Module) + pOffset, 4).Buffer, 0);
        }

        public int ReadInt(int pOffset)
        {
            return BitConverter.ToInt32(this.ReadMem(pOffset, 4).Buffer, 0);
        }

        public int ReadInt(bool AddToImageAddress, int pOffset)
        {
            return BitConverter.ToInt32(this.ReadMem(pOffset, 4, AddToImageAddress).Buffer, 0);
        }

        public int ReadInt(string Module, int pOffset)
        {
            return BitConverter.ToInt32(this.ReadMem(this.DllImageAddress(Module) + pOffset, 4).Buffer, 0);
        }

        public short ReadShort(int pOffset)
        {
            return BitConverter.ToInt16(this.ReadMem(pOffset, 2).Buffer, 0);
        }

        public short ReadShort(bool AddToImageAddress, int pOffset)
        {
            return BitConverter.ToInt16(this.ReadMem(pOffset, 2, AddToImageAddress).Buffer, 0);
        }

        public short ReadShort(string Module, int pOffset)
        {
            return BitConverter.ToInt16(this.ReadMem(this.DllImageAddress(Module) + pOffset, 2).Buffer, 0);
        }

        public string ReadStringAscii(int pOffset, int pSize)
        {
            return MemoryHelper.CutString(Encoding.ASCII.GetString(this.ReadMem(pOffset, pSize).Buffer));
        }

        public string ReadStringAscii(bool AddToImageAddress, int pOffset, int pSize)
        {
            return MemoryHelper.CutString(Encoding.ASCII.GetString(this.ReadMem(pOffset, pSize, AddToImageAddress).Buffer));
        }

        public string ReadStringAscii(string Module, int pOffset, int pSize)
        {
            return MemoryHelper.CutString(Encoding.ASCII.GetString(this.ReadMem(this.DllImageAddress(Module) + pOffset, pSize).Buffer));
        }

        public string ReadStringUnicode(int pOffset, int pSize)
        {
            return MemoryHelper.CutString(Encoding.Unicode.GetString(this.ReadMem(pOffset, pSize).Buffer));
        }

        public string ReadStringUnicode(bool AddToImageAddress, int pOffset, int pSize)
        {
            return MemoryHelper.CutString(Encoding.Unicode.GetString(this.ReadMem(pOffset, pSize, AddToImageAddress).Buffer));
        }

        public string ReadStringUnicode(string Module, int pOffset, int pSize)
        {
            return MemoryHelper.CutString(Encoding.Unicode.GetString(this.ReadMem(this.DllImageAddress(Module) + pOffset, pSize).Buffer));
        }

        public uint ReadUInt(int pOffset)
        {
            return BitConverter.ToUInt32(this.ReadMem(pOffset, 4).Buffer, 0);
        }

        public uint ReadUInt(bool AddToImageAddress, int pOffset)
        {
            return BitConverter.ToUInt32(this.ReadMem(pOffset, 4, AddToImageAddress).Buffer, 0);
        }

        public uint ReadUInt(string Module, int pOffset)
        {
            return BitConverter.ToUInt32(this.ReadMem(this.DllImageAddress(Module) + pOffset, 4).Buffer, 0);
        }

        public void WriteByte(int pOffset, byte pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes((short)pBytes));
        }

        public void WriteByte(bool AddToImageAddress, int pOffset, byte pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes((short)pBytes), AddToImageAddress);
        }

        public void WriteByte(string Module, int pOffset, byte pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes((short)pBytes));
        }

        public void WriteDouble(int pOffset, double pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteDouble(bool AddToImageAddress, int pOffset, double pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteDouble(string Module, int pOffset, double pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteFloat(int pOffset, float pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteFloat(bool AddToImageAddress, int pOffset, float pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteFloat(string Module, int pOffset, float pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteInt(int pOffset, int pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteInt(bool AddToImageAddress, int pOffset, int pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteInt(string Module, int pOffset, int pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteShort(int pOffset, short pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteShort(bool AddToImageAddress, int pOffset, short pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteShort(string Module, int pOffset, short pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteStringAscii(int pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.ASCII.GetBytes(pBytes + "\0"));
        }

        public void WriteStringAscii(bool AddToImageAddress, int pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.ASCII.GetBytes(pBytes + "\0"), AddToImageAddress);
        }

        public void WriteStringAscii(string Module, int pOffset, string pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, Encoding.ASCII.GetBytes(pBytes + "\0"));
        }

        public void WriteStringUnicode(int pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.Unicode.GetBytes(pBytes + "\0"));
        }

        public void WriteStringUnicode(bool AddToImageAddress, int pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.Unicode.GetBytes(pBytes + "\0"), AddToImageAddress);
        }

        public void WriteStringUnicode(string Module, int pOffset, string pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, Encoding.Unicode.GetBytes(pBytes + "\0"));
        }

        public void WriteUInt(int pOffset, uint pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteUInt(bool AddToImageAddress, int pOffset, uint pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteUInt(string Module, int pOffset, uint pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }
    }
}