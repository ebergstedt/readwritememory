﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace ReadWriteMemory
{
    public class ReadWriteMemory64 : ReadWriteBase
    {
        protected long BaseAddress;

        public ReadWriteMemory64(int processID)
            : base(processID)
        {
        }

        public ReadWriteMemory64(string processName)
            : base(processName)
        {
        }

        public long DllImageAddress(string dllname)
        {
            foreach (ProcessModule procmodule in MyProcess.Modules)
            {
                if (dllname == procmodule.ModuleName)
                {
                    return (long)procmodule.BaseAddress;
                }
            }
            return -1;
        }

        public long ImageAddress()
        {
            this.BaseAddress = 0;
            this.MyProcessModule = base.MyProcess.MainModule;
            this.BaseAddress = (long)this.MyProcessModule.BaseAddress;
            return this.BaseAddress;
        }

        public long ImageAddress(long pOffset)
        {
            this.BaseAddress = 0;
            this.MyProcessModule = this.MyProcess.MainModule;
            this.BaseAddress = (long)this.MyProcessModule.BaseAddress;
            return (pOffset + this.BaseAddress);
        }

        public long Point(bool AddToImageAddress, long pOffset)
        {
            return this.ReadLong(this.ImageAddress(pOffset));
        }

        public MemoryHelper.ByteMem64 ReadMem(long pOffset, long pSize)
        {
            byte[] buffer = new byte[pSize];
            long lpNumberOfBytesRead;
            ReadProcessMemory(base.ProcessHandle, pOffset, out buffer, pSize, out lpNumberOfBytesRead);
            return new MemoryHelper.ByteMem64() { Buffer = buffer, LpNumberOfBytesRead = lpNumberOfBytesRead };
        }

        public MemoryHelper.ByteMem64 ReadMem(long pOffset, long pSize, bool AddToImageAddress)
        {
            byte[] buffer = new byte[pSize];
            long lpNumberOfBytesRead;
            long lpBaseAddress = AddToImageAddress ? this.ImageAddress(pOffset) : pOffset;
            ReadProcessMemory(base.ProcessHandle, lpBaseAddress, out buffer, pSize, out lpNumberOfBytesRead);
            return new MemoryHelper.ByteMem64() { Buffer = buffer, LpNumberOfBytesRead = lpNumberOfBytesRead };
        }

        public long WriteMem(long pOffset, byte[] pBytes)
        {
            long lpNumberOfBytesWritten;
            WriteProcessMemory(base.ProcessHandle, pOffset, pBytes, pBytes.Length, out lpNumberOfBytesWritten);
            return lpNumberOfBytesWritten;
        }

        public long WriteMem(long pOffset, byte[] pBytes, bool AddToImageAddress)
        {
            long lpBaseAddress = AddToImageAddress ? this.ImageAddress(pOffset) : pOffset;
            long lpNumberOfBytesWritten;
            WriteProcessMemory(base.ProcessHandle, lpBaseAddress, pBytes, pBytes.Length, out lpNumberOfBytesWritten);
            return lpNumberOfBytesWritten;
        }

        public long Point(string Module, long pOffset)
        {
            return this.ReadLong(this.DllImageAddress(Module) + pOffset);
        }

        public long Point(bool AddToImageAddress, long pOffset, long pOffset2)
        {
            if (AddToImageAddress)
                return (this.ReadLong(this.ImageAddress() + pOffset) + pOffset2);
            else
                return (this.ReadLong(pOffset) + pOffset2);
        }

        public long Point(string Module, long pOffset, long pOffset2)
        {
            return (this.ReadLong(this.DllImageAddress(Module) + pOffset) + pOffset2);
        }

        public long Point(bool AddToImageAddress, long pOffset, long pOffset2, long pOffset3)
        {
            return (this.ReadLong(this.ReadLong(this.ImageAddress(pOffset)) + pOffset2) + pOffset3);
        }

        public long Point(string Module, long pOffset, long pOffset2, long pOffset3)
        {
            return (this.ReadLong(this.ReadLong(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3);
        }

        public long Point(bool AddToImageAddress, long pOffset, long pOffset2, long pOffset3, long pOffset4)
        {
            return (this.ReadLong(this.ReadLong(this.ReadLong(this.ImageAddress(pOffset)) + pOffset2) + pOffset3) + pOffset4);
        }

        public long Point(string Module, long pOffset, long pOffset2, long pOffset3, long pOffset4)
        {
            return (this.ReadLong(this.ReadLong(this.ReadLong(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3) + pOffset4);
        }

        public long Point(bool AddToImageAddress, long pOffset, long pOffset2, long pOffset3, long pOffset4, long pOffset5)
        {
            return (this.ReadLong(this.ReadLong(this.ReadLong(this.ReadLong(this.ImageAddress(pOffset)) + pOffset2) + pOffset3) + pOffset4) + pOffset5);
        }

        public long Point(string Module, long pOffset, long pOffset2, long pOffset3, long pOffset4, long pOffset5)
        {
            return (this.ReadLong(this.ReadLong(this.ReadLong(this.ReadLong(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3) + pOffset4) + pOffset5);
        }

        public long Point(bool AddToImageAddress, long pOffset, long pOffset2, long pOffset3, long pOffset4, long pOffset5, long pOffset6)
        {
            return (this.ReadLong(this.ReadLong(this.ReadLong(this.ReadLong(this.ReadLong(this.ImageAddress(pOffset)) + pOffset2) + pOffset3) + pOffset4) + pOffset5) + pOffset6);
        }

        public long Point(string Module, long pOffset, long pOffset2, long pOffset3, long pOffset4, long pOffset5, long pOffset6)
        {
            return (this.ReadLong(this.ReadLong(this.ReadLong(this.ReadLong(this.ReadLong(this.DllImageAddress(Module) + pOffset) + pOffset2) + pOffset3) + pOffset4) + pOffset5) + pOffset6);
        }

        public byte ReadByte(long pOffset)
        {
            byte[] buffer = new byte[1];
            long lpNumberOfBytesRead;
            ReadProcessMemory(base.ProcessHandle, pOffset, out buffer, 1, out lpNumberOfBytesRead);
            return buffer[0];
        }

        public byte ReadByte(bool AddToImageAddress, long pOffset)
        {
            byte[] buffer = new byte[1];
            long lpNumberOfBytesRead;
            long lpBaseAddress = AddToImageAddress ? this.ImageAddress(pOffset) : pOffset;
            ReadProcessMemory(base.ProcessHandle, lpBaseAddress, out buffer, 1, out lpNumberOfBytesRead);
            return buffer[0];
        }

        public byte ReadByte(string Module, long pOffset)
        {
            byte[] buffer = new byte[1];
            long lpNumberOfBytesRead;
            ReadProcessMemory(base.ProcessHandle, this.DllImageAddress(Module) + pOffset, out buffer, 1, out lpNumberOfBytesRead);
            return buffer[0];
        }

        public float ReadFloat(long pOffset)
        {
            return BitConverter.ToSingle(this.ReadMem(pOffset, 4).Buffer, 0);
        }

        public float ReadFloat(bool AddToImageAddress, long pOffset)
        {
            return BitConverter.ToSingle(this.ReadMem(pOffset, 4, AddToImageAddress).Buffer, 0);
        }

        public float ReadFloat(string Module, long pOffset)
        {
            return BitConverter.ToSingle(this.ReadMem(this.DllImageAddress(Module) + pOffset, 4).Buffer, 0);
        }

        public long ReadLong(long pOffset)
        {
            return BitConverter.ToInt64(this.ReadMem(pOffset, 4).Buffer, 0);
        }

        public long ReadLong(bool AddToImageAddress, long pOffset)
        {
            return BitConverter.ToInt64(this.ReadMem(pOffset, 4, AddToImageAddress).Buffer, 0);
        }

        public long ReadLong(string Module, long pOffset)
        {
            return BitConverter.ToInt64(this.ReadMem(this.DllImageAddress(Module) + pOffset, 4).Buffer, 0);
        }

        public short ReadShort(long pOffset)
        {
            return BitConverter.ToInt16(this.ReadMem(pOffset, 2).Buffer, 0);
        }

        public short ReadShort(bool AddToImageAddress, long pOffset)
        {
            return BitConverter.ToInt16(this.ReadMem(pOffset, 2, AddToImageAddress).Buffer, 0);
        }

        public short ReadShort(string Module, long pOffset)
        {
            return BitConverter.ToInt16(this.ReadMem(this.DllImageAddress(Module) + pOffset, 2).Buffer, 0);
        }

        public string ReadStringAscii(long pOffset, long pSize)
        {
            return MemoryHelper.CutString(Encoding.ASCII.GetString(this.ReadMem(pOffset, pSize).Buffer));
        }

        public string ReadStringAscii(bool AddToImageAddress, long pOffset, long pSize)
        {
            return MemoryHelper.CutString(Encoding.ASCII.GetString(this.ReadMem(pOffset, pSize, AddToImageAddress).Buffer));
        }

        public string ReadStringAscii(string Module, long pOffset, long pSize)
        {
            return MemoryHelper.CutString(Encoding.ASCII.GetString(this.ReadMem(this.DllImageAddress(Module) + pOffset, pSize).Buffer));
        }

        public string ReadStringUnicode(long pOffset, long pSize)
        {
            return MemoryHelper.CutString(Encoding.Unicode.GetString(this.ReadMem(pOffset, pSize).Buffer));
        }

        public string ReadStringUnicode(bool AddToImageAddress, long pOffset, long pSize)
        {
            return MemoryHelper.CutString(Encoding.Unicode.GetString(this.ReadMem(pOffset, pSize, AddToImageAddress).Buffer));
        }

        public string ReadStringUnicode(string Module, long pOffset, long pSize)
        {
            return MemoryHelper.CutString(Encoding.Unicode.GetString(this.ReadMem(this.DllImageAddress(Module) + pOffset, pSize).Buffer));
        }

        public long ReadUlong(long pOffset)
        {
            return BitConverter.ToInt64(this.ReadMem(pOffset, 4).Buffer, 0);
        }

        public long ReadUlong(bool AddToImageAddress, long pOffset)
        {
            return BitConverter.ToInt64(this.ReadMem(pOffset, 4, AddToImageAddress).Buffer, 0);
        }

        public long ReadUlong(string Module, long pOffset)
        {
            return BitConverter.ToInt64(this.ReadMem(this.DllImageAddress(Module) + pOffset, 4).Buffer, 0);
        }

        public void WriteByte(long pOffset, byte pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes((short)pBytes));
        }

        public void WriteByte(bool AddToImageAddress, long pOffset, byte pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes((short)pBytes), AddToImageAddress);
        }

        public void WriteByte(string Module, long pOffset, byte pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes((short)pBytes));
        }

        public void WriteDouble(long pOffset, double pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteDouble(bool AddToImageAddress, long pOffset, double pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteDouble(string Module, long pOffset, double pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteFloat(long pOffset, float pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteFloat(bool AddToImageAddress, long pOffset, float pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteFloat(string Module, long pOffset, float pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void Writelong(long pOffset, long pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void Writelong(bool AddToImageAddress, long pOffset, long pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void Writelong(string Module, long pOffset, long pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteShort(long pOffset, short pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteShort(bool AddToImageAddress, long pOffset, short pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteShort(string Module, long pOffset, short pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteStringAscii(long pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.ASCII.GetBytes(pBytes + "\0"));
        }

        public void WriteStringAscii(bool AddToImageAddress, long pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.ASCII.GetBytes(pBytes + "\0"), AddToImageAddress);
        }

        public void WriteStringAscii(string Module, long pOffset, string pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, Encoding.ASCII.GetBytes(pBytes + "\0"));
        }

        public void WriteStringUnicode(long pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.Unicode.GetBytes(pBytes + "\0"));
        }

        public void WriteStringUnicode(bool AddToImageAddress, long pOffset, string pBytes)
        {
            this.WriteMem(pOffset, Encoding.Unicode.GetBytes(pBytes + "\0"), AddToImageAddress);
        }

        public void WriteStringUnicode(string Module, long pOffset, string pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, Encoding.Unicode.GetBytes(pBytes + "\0"));
        }

        public void WriteUlong(long pOffset, ulong pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes));
        }

        public void WriteUlong(bool AddToImageAddress, long pOffset, ulong pBytes)
        {
            this.WriteMem(pOffset, BitConverter.GetBytes(pBytes), AddToImageAddress);
        }

        public void WriteUlong(string Module, long pOffset, ulong pBytes)
        {
            this.WriteMem(this.DllImageAddress(Module) + pOffset, BitConverter.GetBytes(pBytes));
        }
    }
}