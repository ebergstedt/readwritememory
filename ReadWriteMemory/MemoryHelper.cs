﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ReadWriteMemory
{
    public static class MemoryHelper
    {
        public static uint GetdwDesiredAccess(bool DELETE = true, 
            bool READ_CONTROL = true, 
            bool WRITE_DAC = true, 
            bool WRITE_OWNER = true, 
            bool SYNCHRONIZE = true, 
            bool WindowsXP_or_WindowsServer2003 = false)
        {
            const uint uDELETE = 0x00010000;
            const uint uREAD_CONTROL = 0x00020000;
            const uint uWRITE_DAC = 0x00040000;
            const uint uWRITE_OWNER = 0x00080000;
            const uint uSYNCHRONIZE = 0x00100000;
            var uEnd = WindowsXP_or_WindowsServer2003 ? (uint)0xFFFF : (uint)0xFFF;
            const uint uNull = 0x00000000;
            return ((DELETE ? uDELETE : uNull)
                | (READ_CONTROL ? uREAD_CONTROL : uNull)
                | (WRITE_DAC ? uWRITE_DAC : uNull)
                | (WRITE_OWNER ? uWRITE_OWNER : uNull)
                | (SYNCHRONIZE ? uSYNCHRONIZE : uNull)
                | uEnd);
        }

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 2035711,
            CreateThread = 2,
            DupHandle = 64,
            QueryInformation = 1024,
            SetInformation = 512,
            Synchronize = 1048576,
            Terminate = 1,
            VMOperation = 8,
            VMRead = 16,
            VMWrite = 32
        }

        public static string CutString(string mystring)
        {
            var chArray = mystring.ToCharArray();
            var str = "";
            for (var i = 0; i < mystring.Length; i++)
            {
                if ((chArray[i] == ' ') && (chArray[i + 1] == ' '))
                {
                    return str;
                }
                if (chArray[i] == '\0')
                {
                    return str;
                }
                str = str + chArray[i];
            }
            return mystring.TrimEnd(new char[] { '0' });
        }

        public static List<ProcessModule> GetModuleData(Process process)
        {
            return process.Modules.Cast<ProcessModule>().ToList();
        }

        public static int GetObjectSize(object testObject)
        {
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, testObject);
            byte[] Array = ms.ToArray();
            return Array.Length;
        }

        public struct ByteMem32
        {
            public byte[] Buffer;
            public int LpNumberOfBytesRead;
        }

        public struct ByteMem64
        {
            public byte[] Buffer;
            public long LpNumberOfBytesRead;
        }

    }
}
