##ReadWriteMemory

This is a helper library for reading and writing memory to both 32-bit and 64-bit processes in Windows. Many versions of Windows are supported, as long as they have a usable version of kernel32.dll.

####NOTE: This library does not help you aquire offsets.

You can use it like this (a sample Visual Studio project is included)

    class Program
    {
        static void Main(string[] args)
        {
            var RWM = new ReadWriteMemory64("notepad");
            if (!RWM.OpenProcess(MemoryHelper.GetdwDesiredAccess()))
                return;
            Console.WriteLine(RWM.ImageAddress());
        }
    }

There is also support for writing/reading different types depending on memory offset.

* Float
* Int
* Byte
* Double
* Short
* Unicode string
* ASCII string

##TODO

* Include a memory scanner.
* Figure out a better system for int/long conversions to reduce duplication between 32 and 64-bit classes.